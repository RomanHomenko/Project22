//
//  ViewController.swift
//  Project22
//
//  Created by Роман Хоменко on 25.06.2022.
//

import CoreLocation
import UIKit

class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var distanceReadingLabel: UILabel!
    
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        
        view.backgroundColor = .gray
    }


}

// MARK: - Location manager methods
extension ViewController {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .authorizedAlways {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    startScaning()
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint) {
        if let beacons = beacons.first {
            update(distance: beacons.proximity)
        } else {
            update(distance: .unknown)
        }
    }
}

// MARK: - Helper methods
extension ViewController {
    func startScaning() {
        let uuid = UUID(uuidString: "5A4BCFCE-174E-4BAC-A814-892E77F687E5")!
        let beaconRegion = CLBeaconRegion(uuid: uuid, major: 123, minor: 456, identifier: "MyBeacon")
        
        locationManager?.startMonitoring(for: beaconRegion)
        locationManager?.startRangingBeacons(satisfying: beaconRegion.beaconIdentityConstraint)
    }
    
    func update(distance: CLProximity) {
        UIView.animate(withDuration: 1) {
            switch distance {
            case .immediate:
                self.view.backgroundColor = .red
                self.distanceReadingLabel.text = "RIGHT HERE"
            case .near:
                self.view.backgroundColor = .orange
                self.distanceReadingLabel.text = "NEAR"
            case .far:
                self.view.backgroundColor = .blue
                self.distanceReadingLabel.text = "FAR"
            default:
                self.view.backgroundColor = .gray
                self.distanceReadingLabel.text = "UNKNOWN"
            }
        }
    }
}
